#include "Ellipse.h"

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x = 0;
	int y = b;
	int p = -2 * a * a*b + a*a + 2 * b*b;
		Draw4Points(xc, yc, x, y, ren);
		while (x*x*(a*a + b*b) <= (a*a*a*a)){
			if (p <= 0){
				p += 4 * b*b*x + 6 * b*b;
			}
			else{
				p += 4 * b*b*x - 4 * a*a*y + (4 * a*a + 6 * b*b);
				y = y - 1;
			}
			x = x + 1;
			Draw4Points(xc, yc, x, y, ren);
		}

    // Area 2
		y = 0;
		x = a;
		p = -2 * a * b*b + 2*a*a + b*b;
		Draw4Points(xc, yc, x, y, ren);
		while (a*a*y<b*b*x){
			if (p >= 0){
				p += 4 * b*b*(1-x);
				x = x - 1;
			}
			p += a*a* ((4 * y) + 6);
			y = y + 1;
			Draw4Points(xc, yc, x, y, ren);
		}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
    // Area 1

    // Area 2

}