#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{

	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);

	SDL_RenderDrawPoint(ren, xc + y, yc + x);
	SDL_RenderDrawPoint(ren, xc + y, yc - x);
	SDL_RenderDrawPoint(ren, xc - y, yc + x);
	SDL_RenderDrawPoint(ren, xc - y, yc - x);

	//7 points
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0, y = R;
	int d = 3 - 2 * R;
	while (y >= x)
	{
		SDL_RenderDrawPoint(ren, xc + x, yc + y);
		SDL_RenderDrawPoint(ren, xc - x, yc + y);
		SDL_RenderDrawPoint(ren, xc + x, yc - y);
		SDL_RenderDrawPoint(ren, xc - x, yc - y);
		SDL_RenderDrawPoint(ren, xc + y, yc + x);
		SDL_RenderDrawPoint(ren, xc - y, yc + x);
		SDL_RenderDrawPoint(ren, xc + y, yc - x);
		SDL_RenderDrawPoint(ren, xc - y, yc - x);
		x++;
		if (d > 0)
		{
			y--;
			d = d + 4 * (x - y) + 10;
		}
		else
		{
			d = d + 4 * x + 6;
			SDL_RenderDrawPoint(ren, xc + x, yc + y);
			SDL_RenderDrawPoint(ren, xc - x, yc + y);
			SDL_RenderDrawPoint(ren, xc + x, yc - y);
			SDL_RenderDrawPoint(ren, xc - x, yc - y);
			SDL_RenderDrawPoint(ren, xc + y, yc + x);
			SDL_RenderDrawPoint(ren, xc - y, yc + x);
			SDL_RenderDrawPoint(ren, xc + y, yc - x);
			SDL_RenderDrawPoint(ren, xc - y, yc - x);
		}
	}

}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = R, y = 0;
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	int P = 5 / 4 - R;
	while (x > y)
	{
		y++;
		if (P <= 0)
			P = P + 2 * y + 1;
		else
		{
			x--;
			P = P + 2 * y - 2 * x + 1;
		}
		if (x < y)
			break;
		SDL_RenderDrawPoint(ren, x + xc, y + yc);
		SDL_RenderDrawPoint(ren, -x + xc, y + yc);
		SDL_RenderDrawPoint(ren, x + xc, -y + yc);
		SDL_RenderDrawPoint(ren, -x + xc, -y + yc);
		SDL_RenderDrawPoint(ren, y + xc, x + yc);
		SDL_RenderDrawPoint(ren, -y + xc, x + yc);
		SDL_RenderDrawPoint(ren, y + xc, -x + yc);
		SDL_RenderDrawPoint(ren, -y + xc, -x + yc);
	}

}
