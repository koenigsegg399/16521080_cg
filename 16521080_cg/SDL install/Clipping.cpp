#include "Clipping.h"
RECT CreateWindow(int l, int r, int t, int b)
{
	RECT rect;
	rect.Left = l;
	rect.Right = r;
	rect.Top = t;
	rect.Bottom = b;

	return rect;
}

CODE Encode(RECT r, Vector2D P)
{
	CODE c = 0;
	if (P.x < r.Left)
		c = c | LEFT;
	if (P.x > r.Right)
		c = c | RIGHT;
	if (P.y < r.Top)
		c = c | TOP;
	if (P.y > r.Bottom)
		c = c | BOTTOM;
	return c;
}
int CheckCase(int c1, int c2)
{
	if (c1 == 0 && c2 == 0)
		return 1;
	if ((c1 != 0) && (c2 != 0) && ((c1&c2) != 0))
		return 2;
	return 3;
}
int CohenSutherland(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	unsigned int code1 = Encode(r, P1);
	unsigned int code2 = Encode(r, P2);
	if (CheckCase(code1, code2) == 1) //th1
	{
		Q1.x = P1.x;
		Q1.y = P1.y;
		Q2.x = P2.x;
		Q2.y = P2.y;
		return 1; //accept
	}
	if (CheckCase(code1, code2) == 2)  //TH2
	{
		return 0; // not accepted
	}
	ClippingCohenSutherland(r, Q1, Q2);
	return 1;
}
//
void ClippingCohenSutherland(RECT r, Vector2D &P1, Vector2D &P2)
{
	CODE code1 = Encode(r, P1);
	CODE code2 = Encode(r, P2);
	while (CheckCase(code1, code2) == 3)
	{
		if (CheckCase(code1, code2) == 1) return;
		if (CheckCase(code1, code2) == 2) return;
		CODE codeOut = code1; //codeOut is the encode of the outside point
		if (code1 == 0)  //if P1 inside the retangle
		{
			codeOut = code2;
		}
		/*
		x=P1.x + (P2.x-P1.x)*t
		y=P1.y + (P2.y-P1.y)*t
		*/
		CODE newX, newY;
		if (codeOut & 1)
		{
			newX = r.Left;
			//calculating parameter t
			double t = (newX - P1.x) / (P2.x - P1.x);
			newY = P1.y + (P2.y - P1.y)*t;

		}
		else
		if (codeOut & 2)
		{
			newX = r.Right;
			//calculating parameter t
			double t = (newX - P1.x) / (P2.x - P1.x);
			newY = P1.y + (P2.y - P1.y)*t;
		}
		else
		if (codeOut & 4)
		{
			newY = r.Top;
			//calculating parameter t
			double t = (newY - P1.y) / (P2.y - P1.y);
			newX = P1.x + (P2.x - P1.x)*t;
		}
		else
		if (codeOut & 8)
		{
			newY = r.Bottom;
			//calculating parameter t
			double t = (newY - P1.y) / (P2.y - P1.y);
			newX = P1.x + (P2.x - P1.x)*t;
		}
		//=====
		if (codeOut == code1)
		{
			P1.x = newX;
			P1.y = newY;
			code1 = Encode(r, P1);
		}
		if (codeOut == code2)
		{
			P2.x = newX;
			P2.y = newY;
			code2 = Encode(r, P2);
		}
	}
}
//
int SolveNonLinearEquation(int p, int q, float &t1, float &t2)
{
	if (p == 0)
	{
		if (q < 0)
			return 0;
		return 1;
	}

	if (p > 0)
	{
		float t = (float)q / p;
		if (t2<t)
			return 1;
		if (t<t1)
			return 0;
		t2 = t;
		return 1;
	}

	float t = (float)q / p;
	if (t2<t)
		return 0;
	if (t<t1)
		return 1;
	t1 = t;
	return 1;
}
//
int LiangBarsky(RECT r, Vector2D P1, Vector2D P2, Vector2D &Q1, Vector2D &Q2)
{
	int i, gm;
	int x1, y1, x2, y2, xmin, xmax, ymin, ymax, xx1, xx2, yy1, yy2, dx, dy;
	float t1, t2, p[4], q[4], temp;
	x1 = P1.x;
	y1 = P1.y;
	x2 = P2.x;
	y2 = P2.y;
	xmin = r.Left;
	ymin = r.Bottom;
	xmax = r.Right;
	ymax = r.Top;
	dx = x2 - x1;
	dy = y2 - y1;

	p[0] = -dx;
	p[1] = dx;
	p[2] = -dy;
	p[3] = dy;

	q[0] = x1 - xmin;
	q[1] = xmax - x1;
	q[2] = y1 - ymin;
	q[3] = ymax - y1;

	for (i = 0; i<4; i++)
	{
		if (p[i] == 0)
		{
			if (q[i] >= 0)
			{
				if (i<2)
				{
					if (y1<ymin)
					{
						y1 = ymin;
					}

					if (y2>ymax)
					{
						y2 = ymax;
					}
				}

				if (i>1)
				{
					if (x1<xmin)
					{
						x1 = xmin;
					}

					if (x2>xmax)
					{
						x2 = xmax;
					}
				}
			}
		}
	}

	t1 = 0;
	t2 = 1;

	for (i = 0; i<4; i++)
	{
		temp = q[i] / p[i];

		if (p[i]<0)
		{
			if (t1 <= temp)
				t1 = temp;
		}
		else
		{
			if (t2>temp)
				t2 = temp;
		}
	}

	if (t1<t2)
	{
		Q1.x = x1 + t1 * p[1];
		Q2.x = x1 + t2 * p[1];
		Q1.y = y1 + t1 * p[3];
		Q2.y = y1 + t2 * p[3];
	}
	return 0;
}

