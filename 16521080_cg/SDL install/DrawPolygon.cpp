#include "DrawPolygon.h"
#include <iostream>
using namespace std;
#define PI 3.14159265

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[3], y[3];
	float phi = PI / 2;
	for (int i = 0; i < 3; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 3;
	}
	for (int i = 0; i < 3; i++){
		Bresenham_Line(x[i], y[i], x[(i+1)%3], y[(i+1)%3], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[4], y[4];
	float phi = -PI / 4;
	for (int i = 0; i < 4; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 4;
	}
	for (int i = 0; i < 4; i++){
		Bresenham_Line(x[i], y[i], x[(i + 1) % 4], y[(i + 1) % 4], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++){
		Bresenham_Line(x[i], y[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[6], y[6];
	float phi = PI / 2;
	for (int i = 0; i < 6; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 6;
	}
	for (int i = 0; i < 6; i++){
		Bresenham_Line(x[i], y[i], x[(i + 1) % 6], y[(i + 1) % 6], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++){
		Bresenham_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[5], y[5];
	int x1[5], y1[5];
	float xxx = (tan(3 * PI / 10) / tan(2 * PI / 5));
	int r = (R*(1 - xxx) / (1 + xxx));

	float phi = PI / 2;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}



    phi = 7*PI / 10;
	for (int i = 0; i < 5; i++)
	{
		x1[i] = xc + int(r*cos(phi) + 0.5);
		y1[i] = yc - int(r*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++){
		Bresenham_Line(x[i], y[i], x1[i], y1[i], ren);
		Bresenham_Line(x1[i], y1[i], x[(i + 1) % 5], y[(i + 1) % 5], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x[8], y[8];
	float phi = PI / 2;
	for (int i = 0; i < 8; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 8;
	}
	for (int i = 0; i < 8; i++){
		Bresenham_Line(x[i], y[i], x[(i + 3) % 8], y[(i + 3) % 8], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, float startAngle, SDL_Renderer *ren)
{
	int x[5], y[5];
	float phi = PI / 2 + startAngle;
	for (int i = 0; i < 5; i++)
	{
		x[i] = xc + int(R*cos(phi) + 0.5);
		y[i] = yc - int(R*sin(phi) + 0.5);
		phi += 2 * PI / 5;
	}
	for (int i = 0; i < 5; i++){
		Bresenham_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	int x[5], y[5];
	float xxx = (tan(3 * PI / 10) / tan(2 * PI / 5));
	float phi = PI / 2;
	while (r > 1){
		for (int i = 0; i < 5; i++)
		{
			x[i] = xc + int(r*cos(phi) + 0.5);
			y[i] = yc - int(r*sin(phi) + 0.5);
			phi += 2 * PI / 5;
		}
		for (int i = 0; i < 5; i++){
			Bresenham_Line(x[i], y[i], x[(i + 2) % 5], y[(i + 2) % 5], ren);
		}
		r = (r*(1 - xxx) / (1 + xxx));
		phi = phi + PI / 5;
	}
}