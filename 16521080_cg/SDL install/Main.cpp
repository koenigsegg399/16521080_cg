#include <iostream>
#include <SDL.h>
#include "Bezier.h"
#include"FillColor.h"
#include"Circle.h"
using namespace std;

const int WIDTH = 800;
const int HEIGHT = 1000;

SDL_Event event;


int main(int, char**){
	//First we need to start up SDL, and make sure it went ok
	if (SDL_Init(SDL_INIT_VIDEO) != 0){
		std::cout << "SDL_Init Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("Hello World!", 0, 0, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	//Make sure creating our window went ok
	if (win == NULL){
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return 1;
	}

	//Create a renderer that will draw to the window, -1 specifies that we want to load whichever
	//DON'T FORGET CHANGE THIS LINE IN YOUR SOURCE ----->>>>> SDL_RENDERER_SOFTWARE

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_SOFTWARE);
	if (ren == NULL){
		SDL_DestroyWindow(win);
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		SDL_Quit();
		return 1;
	}

	SDL_SetRenderDrawColor(ren, 0, 0, 0, 255);
	SDL_RenderClear(ren);

	//YOU CAN INSERT CODE FOR TESTING HERE

	Uint32 pixel_format = SDL_GetWindowPixelFormat(win);



	SDL_Color fillcolor;
	fillcolor.r = 0;
	fillcolor.g = 0;
	fillcolor.b = 255;
	fillcolor.a = 255;

	SDL_Color boundary;
	boundary.r = 255;
	boundary.g = 0;
	boundary.b = 0;
	boundary.a = 255;

	Vector2D O(100, 100);
	SDL_SetRenderDrawColor(ren, boundary.r, boundary.g, boundary.b, boundary.a);
	
	
	//BresenhamDrawCircle(100, 100, 4, ren);
	//FillIntersectionEllipseCircle(100, 100, 30,20, 110, 110, 20, ren, fillcolor);
	
	SDL_SetRenderDrawColor(ren, 0, 255, 255, 255);
	Vector2D p1(200, 200);
	Vector2D p2(350, 250);
	Vector2D p3(300, 500);
	Vector2D p4(150, 600);
	Vector2D p;
	TriangleFill(p1,p2,p3,ren,fillcolor);
	DrawCurve2(ren, p1, p2, p3);
	DrawCurve3(ren, p1, p2, p3, p4);



	SDL_RenderPresent(ren);
	//Take a quick break after all that hard work
	//Quit if happen QUIT event
	bool running = true;

	while (running)
	{
		//If there's events to handle
		if (SDL_PollEvent(&event))
		{

			//If the user has Xed out the window
			if (event.type == SDL_QUIT)
			{
				//Quit the program
				running = false;
			}
		}

	}

	SDL_DestroyRenderer(ren);
	SDL_DestroyWindow(win);
	SDL_Quit();

	return 0;
}
