#include "Parapol.h"
#include<math.h>
void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}
//void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
//{
//	int w, h;
//	float a = 1 / (2 * A);
//	double p = a - 1 / 2;
//
//	SDL_GetRendererOutputSize(ren, &w, &h);
//	int x, y, d, p2, p4;
//	p2 = 2 * p;      p4 = p2 * 2;
//	x = 0; y = 0;
//	d = 1 - p;
//	while ((y < A) && (x<=w && y<=h))
//	{
//		Draw2Points(xc, yc, x, y, ren);
//		if (d >= 0)
//		{
//			x++;
//			d -= p2;
//		}
//		y++;
//		d += 2 * y + 1;
//	}
//	if (d == 1) d = 1 - p4;
//	else d = 1 - p2;
//	while (x<=w && y<=h)
//	{
//		Draw2Points(xc, yc, x, y,ren);
//		if (d <= 0)
//		{
//			y++;
//			d += 4 * y;
//		}
//		x++;
//		d -= p4;
//	}
//}

void BresenhamDrawParapolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	float a = 1 / (2 * A);
	int x = 0;
	int y = 0;
	double p = a - 1 / 2;
	Draw2Points(xc, yc, x, y, ren);
	while (x < A)
	{
		if (p <= 0)
		{
			p = p + 2 * A - 1 - 2 * x;
			y++;
		}
		else
		{
			p = p - 2 * x - 1;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}
	x = A;
	y = A / 2;
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);
	while (x>0 && y>0 && x<w && y<h)
	{
		if (p<0)
		{
			p = p + 2 * sqrt(double(2 * A))*(sqrt(double(y + 1)) - sqrt(double(y)));
		}
		else
		{
			p = p + 2 * sqrt(double(2 * A))*(sqrt(double(y + 1)) - sqrt(double(y))) - 2;
			x = x + 1;
		}
		y = y + 1;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParapolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
}